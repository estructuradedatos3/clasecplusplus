-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-11-2021 a las 18:18:14
-- Versión del servidor: 10.4.21-MariaDB
-- Versión de PHP: 8.0.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `db_venta_de_casa`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `duenyos`
--

CREATE TABLE `duenyos` (
  `nombre_completo` varchar(100) DEFAULT NULL,
  `cedula` varchar(100) DEFAULT NULL,
  `telefono` int(11) DEFAULT NULL,
  `whatsapp` int(11) DEFAULT NULL,
  `telegram` int(11) DEFAULT NULL,
  `idduenyo` int(11) DEFAULT NULL,
  `idvivi` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `duenyos`
--

INSERT INTO `duenyos` (`nombre_completo`, `cedula`, `telefono`, `whatsapp`, `telegram`, `idduenyo`, `idvivi`) VALUES
('Mario Cabreja', '12345678901', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Pablo Marrero', '12345678902', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Haydée López', '12345678903', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Alejandra Gómez', '12345678904', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Haydée López', '12345678905', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Alejandra Gómez', '12345678906', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Pablo Marrero', '12345678907', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Alejandra Gómez', '12345678908', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Haydée López', '12345678909', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Mario Cabreja', '12345678910', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Pablo Marrero', '12345678911', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Pablo Marrero', '12345678912', 2147483647, 2147483647, 2147483647, NULL, NULL),
('Mario Cabreja', '12345678913', 2147483647, 2147483647, 2147483647, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubicacion`
--

CREATE TABLE `ubicacion` (
  `idubic` int(10) DEFAULT NULL,
  `provincia` varchar(100) DEFAULT NULL,
  `municipio` varchar(100) DEFAULT NULL,
  `casa` varchar(10) DEFAULT NULL,
  `residencia` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vivienda`
--

CREATE TABLE `vivienda` (
  `precio` int(11) DEFAULT NULL,
  `precio_publico` int(11) DEFAULT NULL,
  `medios_banyos` varchar(10) DEFAULT NULL,
  `banyos` varchar(10) DEFAULT NULL,
  `condicion` varchar(10) DEFAULT NULL,
  `metro_construccion` varchar(100) DEFAULT NULL,
  `metro_solar` varchar(100) DEFAULT NULL,
  `vehiculo_marquesina` int(11) DEFAULT NULL,
  `sala` varchar(10) DEFAULT NULL,
  `comedor` varchar(10) DEFAULT NULL,
  `cocina` varchar(10) DEFAULT NULL,
  `amueblada` varchar(10) DEFAULT NULL,
  `idvivi` int(11) DEFAULT NULL,
  `idubic` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
