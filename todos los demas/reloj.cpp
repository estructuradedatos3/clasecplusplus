#include <iostream>
#include <windows.h>
using namespace std;

int main(int argc, char *argv[]) {
	int segundos = 0;
	int minutos = 0;
	int horas = 0;
	bool repetir = true;
	
	while (repetir){
		system("cls");
		cout <<horas<<":"<<minutos<<":"<<segundos<<endl;
		Sleep(1000);
		segundos++;
		
		if(segundos == 60){
			minutos++;
			segundos = 0;
		}
		if(minutos == 60){
			horas++;
			minutos = 0;
		}
		if (horas == 24){
			horas = 0;
			minutos = 0;
			segundos = 0;
		}
	}
	return 0;
}

