#include <iostream>
using namespace std;

struct empleado{
	string nombreCompleto;
	int edad;
};

int main(int argc, char *argv[]) {
	
	empleado  empleado1;
	
	empleado *puntero_empleado1;
	puntero_empleado1 = &empleado1;
	
	puntero_empleado1 -> nombreCompleto = "Paquita la del barrio";
	puntero_empleado1 -> edad = 25;
	
	cout<< puntero_empleado1 -> nombreCompleto<<endl;
	cout<< puntero_empleado1 -> edad <<endl;
	
	return 0;
}

