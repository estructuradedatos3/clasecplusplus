#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	
	float total = 0;
	float descuento = 0;
	
	cout << "Introduzca el monto a pagar" << endl;
	cin >> total;
	
	if (total < 500){
		cout << "No aplica descuento" <<endl;
	}
	
	if (total > 499 and total < 1000) {
		descuento = total * 5 /100;
		cout << total - descuento << " Monto a pagar" << endl;
	}
	
	if (total > 1000 and total < 7000){
		descuento = total * 11 /100;
		cout << total - descuento << " Monto a pagar" << endl;
	}
	
	if (total > 7000 and total < 15000){
		descuento = total * 18 /100;
		cout << total - descuento << " Monto a pagar" << endl;
	}
	
	if (total > 14999){
		descuento = total * 25 /100;
		cout << total - descuento << " Monto a pagar" << endl;
	}
	
	return 0;
}

