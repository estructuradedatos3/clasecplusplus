#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	int matrizdetres [3][3]; //matriz bidimencional 3x3 que almacenara nuestros valores
	matrizdetres[0][0]=10;
	matrizdetres[0][1]=15;
	matrizdetres[0][2]=11;
	
	matrizdetres[1][0]=40;
	matrizdetres[1][1]=30;
	matrizdetres[1][2]=20;
	
	matrizdetres[2][0]=20;
	matrizdetres[2][1]=50;
	matrizdetres[2][2]=5;
	 
	for(int fila = 0; fila<=2; fila++){ //for para ir contando las filas de nuestra matriz
		for(int columna = 0; columna<=2; columna++){ //for para ir contando las columnas de nuestra matriz 
			cout<<matrizdetres[fila][columna]<<endl; //letrero que imprimira las posiciones 
		}
	}
	return 0;
}

