#include <iostream>
#include <iomanip>
using namespace std;

int main(int argc, char *argv[]) {
	int matriztabla [13][13]; //para poder imprimir el 0 declaramos nuestra matriz 13x13
	
	for(int fila = 0; fila<=12; fila++){ //for para ir contando nuestras filas
		for(int columna = 0; columna<=12; columna++){ //for para ir contando nuestras columans
			matriztabla[fila][columna]= fila*columna; //operacion que multiplicara posiciones.
		}
	}
	for(int fila = 0; fila<13; fila++){ //for para ir imprimiendo las filas
		for(int columna = 0; columna<13; columna++){ //for para ir imprimiendo las columnas
		
			cout<<matriztabla[fila][columna]<<setw(5); //letrero para imprimir las multiplicaciones con una separacion de 5
		}
		cout<<"\n";
		cout<<"*************************************************************"<<endl;
	}
	return 0;
}

