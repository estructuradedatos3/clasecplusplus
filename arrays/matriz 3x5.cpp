#include <iostream>
#include <string>
using namespace std;

int main(int argc, char *argv[]) {
	
	string matriztrecinco [5][5]; //matris bidimencional 5x5 para almacenar nuestros textos
	
	
	for( int fila =0; fila<5; fila++){ //for para ir contando las fiilas
		for(int columna =0; columna<5; columna++){ //for para ir contando las columnas 
			cout<<"Favor introduzca el nombre y el apellido: "<<endl; //letrero para pedir ingreso de texto
			getline(cin,matriztrecinco[fila][columna]); //por la dimension del texto "nombre y apellido" utilizamos un getline
			
			break;
		}
	}
	cout<<"======================================================================"<<endl;
	cout<<"Lista de nombres"<<endl;
	for( int fila =0; fila<5; fila++){ //for para ir contando las filas a imprimir
		for(int columna =0; columna<5; columna++){ //for para ir contando las columnas a imprimir 
			cout<<"Nombre de la posicion de "<<fila<<" "<<columna<<": "<<matriztrecinco[fila][columna]<<endl; //impresion en orden.
		}
	}
	return 0;
}

