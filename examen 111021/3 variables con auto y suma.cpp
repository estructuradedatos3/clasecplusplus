#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	//variables de tipo auto para identificar el tipo por el valor introducido
	auto vari = 20; 
	auto segunda = 30;
	auto tercera = 10;
	
	//variable contador de tipo entero
	int suma = 0;
	
	//operacion para sumar las variables 
	suma = vari + segunda + tercera;
	
	//impresion por pantalla
	cout<<suma<<endl;
	
	
	
	return 0;
}
