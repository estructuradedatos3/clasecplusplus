#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	
	cout<<"El tama�o de bites de Char es: "<< sizeof (char)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de Unsigned Char es: "<< sizeof (unsigned char)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de Signed Char es: "<< sizeof (signed char)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de Int es: "<< sizeof (int)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de unsigned int es: "<< sizeof (unsigned int)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de signed int es: "<< sizeof (signed int)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de short int es: "<< sizeof (short int)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de unsigned short int es: "<< sizeof (unsigned short int)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de signed short int es: "<< sizeof (signed short int)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de long int es: "<< sizeof (long int)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de signed long  int es: "<< sizeof (signed long int)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de unsigned long int es: "<< sizeof (unsigned long int)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de float es: "<< sizeof (float)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de double es: "<< sizeof (double)<<endl;
	cout<<'\n';
	cout<<"El tama�o de bites de long double es: "<< sizeof (long double)<<endl;
	
	return 0;
}
