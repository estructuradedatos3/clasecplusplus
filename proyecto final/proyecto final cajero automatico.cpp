#include <iostream>
#include <stdlib.h>
using namespace std;

int main(int argc, char *argv[]) {
	
	long long int cuenta = 0; //Variable de cuenta larga para poder almacenar el digito de cuenta.
	int pin = 0; //Variable para la contraseņa de la cuenta 
	int op = 0; //Opcion para nuestro Switch
	int retiro = 0; //monto que sera retirado
	int consulta = 0; //opcion de consulta
	long long int residuo = 0; //sobrante despues del retiro
	long long int excedente = 0; //para ir debitando de la cuenta
	int op2 = 0;
	long long int montoauto = 0;
	int fila = 0;
	long long int matrizCuentas [4][3]; //Matriz 4x3 para almacear nuestras cuentas
		 matrizCuentas [0][0] = 100000000001;
		 matrizCuentas [0][1] = 1234;
		 matrizCuentas [0][2] = 100000;
		
		 matrizCuentas [1][0] = 100000000002;
		 matrizCuentas [1][1] = 5678;
		 matrizCuentas [1][2] = 90050000;
		
		 matrizCuentas [2][0] = 100000000003;
		 matrizCuentas [2][1] = 9012; 	
		 matrizCuentas [2][2] = 700000;
		
		 matrizCuentas [3][0] = 100000000004;
	     matrizCuentas [3][1] = 9012;
		 matrizCuentas [3][2] = 12000;
		
		 
		 
		 do { //Primer bucle para mantener el ciclo de opciones infinitamente y que no se rompa hasta presionar "salir"
			 
			//seccion para login
		    cout<<"Favor introducir su numero de cuenta: "<<endl; 
			cin>>cuenta;
			cout<<"Favor introducir sun PIN de 4 digitos" <<endl;
			cin>>pin;
		 
			
		
			
			
			for ( fila = 0; fila < 4; fila++){ //bucle for para recorrer las filas
				
				
					if (cuenta == matrizCuentas[fila][0] and pin == matrizCuentas[fila][1]){ //primer condicional para que mientras nuestro login sea correcto entre a nuestro segundo bucle
						
						//segundo bucle que englobara toda nuestra operacion desde retiro de efectivo, consultas de balances y salir
						do{
						cout<<"Que operacion desea realizar? "<<endl;
						cout<<"1 Retiro de efectivo "<<"||"<<" 2 Consulta de balance"<<"||"<<" 3 Salir"<<endl;
						cout<<"============================================================="<<endl;
						cin>>op;
						switch (op){ //nuestro switch/case que contiene los montos y tipos de operaciones en tipo menu, es decir por opciones
						case 1:
							matrizCuentas [fila][2] = matrizCuentas [fila][2] - retiro; 
							cout<<"Por favor escoja la opcion: "<<endl;
							cout<<"Opcion 1: $100.00"<<endl;
							cout<<"Opcion 2: $200.00"<<endl;
							cout<<"Opcion 3: $500.00"<<endl;
							cout<<"Opcion 4: $1000.00"<<endl;
							cout<<"Opcion 5: $2000.00"<<endl;
							cout<<"Opcion 6: Otra cantidad"<<endl;
							cin>>op2;
							if (op2 == 1 and matrizCuentas[fila][2] >= 100){ //primera condicional para debitar el monto por opcion automatica de la cuenta y guardarlo.
								matrizCuentas[fila][2] = matrizCuentas[fila][2]-100;
							cout<<"*************************************************************"<<endl;
							cout<<"El monto a retirar es: $"<<"100" <<endl;
							cout<<"*************************************************************"<<endl;
							cout<<"Balance disponible $"<<matrizCuentas[fila][2]<<endl;
							break;
							}else if (op2 == 2 and matrizCuentas[fila][2] >= 200){ //segunda condicional para debitar el monto por opcion automatica de la cuenta y guardarlo.
								matrizCuentas[fila][2] = matrizCuentas[fila][2]-200;
								cout<<"*************************************************************"<<endl;
								cout<<"El monto a retirar es: $"<<"200" <<endl;
								cout<<"*************************************************************"<<endl;
								cout<<"Balance disponible $"<<matrizCuentas[fila][2]<<endl;
								break;
							}else if (op2 == 3 and matrizCuentas[fila][2] >= 500){ //tercera condicional para debitar el monto por opcion automatica de la cuenta y guardarlo.
								matrizCuentas[fila][2] = matrizCuentas[fila][2]-500;
								cout<<"*************************************************************"<<endl;
								cout<<"El monto a retirar es: $"<<"500" <<endl;
								cout<<"*************************************************************"<<endl;
								cout<<"Balance disponible $"<<matrizCuentas[fila][2]<<endl;
								break;
							}else if (op2 == 4 and matrizCuentas[fila][2] >= 1000){ //cuarta condicional para debitar el monto por opcion automatica de la cuenta y guardarlo.
								matrizCuentas[fila][2] = matrizCuentas[fila][2]-1000;
								cout<<"*************************************************************"<<endl;
								cout<<"El monto a retirar es: $"<<"1000" <<endl;
								cout<<"*************************************************************"<<endl;
								cout<<"Balance disponible $"<<matrizCuentas[fila][2]<<endl;
								break;
							}else if (op2 == 5 and matrizCuentas[fila][2] >= 2000){ //quinta condicional para debitar el monto por opcion automatica de la cuenta y guardarlo.
								matrizCuentas[fila][2] = matrizCuentas[fila][2]-2000;
								cout<<"*************************************************************"<<endl;
								cout<<"El monto a retirar es: $"<<"2000" <<endl;
								cout<<"*************************************************************"<<endl;
								cout<<"Balance disponible $"<<matrizCuentas[fila][2]<<endl;
								break;
							}else if (op2 == 6 and matrizCuentas[fila][2] >= retiro){ //sexta condicional para debitar el monto por opcion automatica de la cuenta y guardarlo.
							cout<<"Favor digitar el monto a retirar "<<endl;

							cin>>retiro;
							cout<<"*************************************************************"<<endl;
							cout<<"El monto a retirar es: $"<<retiro <<endl;
							
							matrizCuentas[fila][2] = matrizCuentas[fila][2]- retiro;
						
							cout<<"*************************************************************"<<endl;
							cout<<"Balance disponible $"<<matrizCuentas[fila][2]<<endl;
							
							cout<<"*************************************************************"<<endl;
							break;
							}
							
						case 2: //case para consulta de balance 
								  
							cout<<"Su balance actual es: "<<matrizCuentas[fila][2] <<endl;
							cout<<"============================================================="<<endl;
							break;
							
						case 3: //case para salir
							cout <<"Hasta pronto!"<<endl;
						exit(0);	break; 
						}
				
          
			}while (op !=3); //while que nos indica que mientras no se precione salir siga iterando nuestro bucle
		}
	
		 }  cout<<"La cuenta y/o el pin son incorrectos"<<endl;
			}
				while(cuenta != matrizCuentas[fila][0] and pin != matrizCuentas[fila][1]); // while que nos indica que si el login no es igual a la posicion de fila y columna no entre a nuestro bucle
	
	return 0;
}

