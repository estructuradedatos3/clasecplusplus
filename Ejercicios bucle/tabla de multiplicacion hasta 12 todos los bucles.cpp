#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	
	int v1 = 3;
	cout <<"Bucle FOR "<<endl;
	cout<<"****************************************************"<<endl; //letrero decorativo
	cout<<"Tablas de multiplicar hasta el 12 "<<endl; //letrero de informacion
	
	for (int tabla = 1; tabla <=12; tabla++){ //primer bucle para ir buscando la tabla
		for (int mult = 1; mult <=12; mult++){ // segundo bucle para buscar el multiplicador
			int resultado = tabla*mult; // operacion de multiplicacion 
			cout<<"Multiplicando "<<tabla<<" x "<<mult<<" = "<<resultado<<endl; //resultado 
		}cout<<"===========================================" <<endl; //barra decorativa
	} 
	cout<<"Bucle While "<<endl;
	cout<<"===========================================" <<endl;
	int i = 0;
	while (i<=12){ //bucle while para nuestra tabla del 12
		int resultado  = (v1 * i); //variable inicializada en operacion por i
		cout <<v1<<" x "<<i<<" = "<<resultado<<endl;
		i++; //contador que ira incrementando el digito a multiplicar
	}
	cout<<"===========================================" <<endl;
	cout<<"Bucle Do while "<<endl;
	cout<<"===========================================" <<endl;
	int a = 0;
	do{ //bucle do while para nuestra tabla del 12
		int resultado = (v1 * a); //variabli inicializada en multiplicacion por a
		cout <<v1<<" x "<<a<<" = "<<resultado<<endl;
		a++; //contador para ir incrementando el valor de a
	}
	while (a<=12); //condicional de nuestro bucle que nos indica que seguira multiplicando hasta que a llegue a 12.
	
	return 0;
}

