#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	string nombre = ""; //variable para nombre
	int edad = 0;      //variable para edad
	
	cout<<"Favor introducir su nombre: "; //letrero para pedir nombre
	cin>>nombre; //para introducir el nombre
	cout<<"Favor introducir su edad: ";//letrero para pedir edad
	cin>>edad;//para introducir edad
	if (edad >= 17){ //inicio de condicion para determinar si la edad introducida es mayor o igual a 17
		cout<<"Hola "<<nombre<<" usted es mayor de edad"<<endl;//letrero de ser positivo
	}
	else {
		cout<<"Hola "<<nombre<<" usted es menor de edad"<<endl;//letrero de ser negativo
	}
	return 0;
}

