#include <iostream>
using namespace std;

int main(int argc, char *argv[]) {
	
	int v1 = 3;
	cout <<"Bucle FOR "<<endl;
	cout<<"****************************************************"<<endl; //letrero decorativo
	cout<<"Tablas de multiplicar hasta el 12 "<<endl; //letrero de informacion
	
	for (int tabla = 1; tabla <=12; tabla++){ //primer bucle para ir buscando la tabla
		for (int mult = 1; mult <=12; mult++){ // segundo bucle para buscar el multiplicador
			int resultado = tabla*mult; // operacion de multiplicacion 
			cout<<"Multiplicando "<<tabla<<" x "<<mult<<" = "<<resultado<<endl; //resultado 
		}cout<<"===========================================" <<endl; //barra decorativa
	} 
	cout<<"Bucle While "<<endl;
	cout<<"===========================================" <<endl;
	int i = 0;
	while (i<=12){ //bucle while para ir multiplicando hasta 12
		int resultado  = (v1 * i); //variable inicializada en multiplicacion por i
		cout <<v1<<" x "<<i<<" = "<<resultado<<endl;
		i++; //contador para incrementar el valor de i
	}
	
	return 0;
}

